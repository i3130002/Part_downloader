﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.Text = Clipboard.GetText();
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            sv.ShowDialog();
            richTextBox2.Text = sv.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            downloader.stop();
            childThread.Join();
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button2.Enabled = true;
            if(proxy_address_tb.Text .Length <1)
                downloader = new Downloader(richTextBox1.Text, richTextBox2.Text, (int)numericUpDown1.Value,(int) numericUpDown2.Value);
            else
                downloader = new Downloader(richTextBox1.Text, richTextBox2.Text, (int)numericUpDown1.Value, (int)numericUpDown2.Value,proxy_address_tb.Text ,int.Parse(proxy_port_tb.Text) ,proxy_lan_chb.Checked );
            childThread = new System.Threading.Thread(downloader.start_downloading);
            childThread.Name = "downloader thread";
            childThread.Start();

            while (childThread.IsAlive)
            {
                childThread.Join(100);
                
                progressBar1.Value = (int)downloader.precentage;
                log_lb.Text = "Total:" + downloader.total_my_part_size + " Remained:" + downloader.total_my_part_remained;
                logs_rt.Text = downloader.logs;
                Application.DoEvents();
                
            }

            button2.Enabled = false;
           button1.Enabled = true;
        }
        System.Threading.Thread childThread;
        Downloader downloader;

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("فایل اول را انتخاب کنید. مطما باشید که فایل ها در کنار یکدیگر هستند");
            OpenFileDialog of = new OpenFileDialog();
                of.ShowDialog();
            StreamReader sr = new StreamReader(of.FileName);
            List<string[]> file_info = new List<string[]>();

            char [] tmp = new char[10];
             sr.Read(tmp, 0, 10);
            for (int i = 0; i < 7; i++)
            {

                string[] data = new string[2];
                string line = sr.ReadLine();
                data[0] = (line.Split('=')[0]);
                data[1]= line.Split('=')[1];
            file_info.Add(data);

            }
            sr.Close();

            FileStream res = File.OpenWrite(Path.GetDirectoryName(of.FileName)+"\\"+ Path.GetFileName(file_info[0][1]));
            int total_parts =int.Parse( file_info[1][1]);

            string pure_file_name = of.FileName.Substring(0,of.FileName.Length-2);

            byte[] buffer = new byte[128 * 1024];
            int bytes = 0;
            for(int i = 0; i< total_parts; i++)
            {
                //read and write
                      //sr = new StreamReader(pure_file_name+"_"+(int)(i+1));
                FileStream fs_ = File.OpenRead(pure_file_name + "_" + (int)(i + 1));
                   bytes= fs_.Read(buffer, 0, 10);
                int skipers =int.Parse( Encoding.UTF8.GetString(buffer, 0, 10));

                fs_.Read(buffer, 0, skipers);
                do
                {
                   bytes= fs_.Read(buffer, 0, 128 * 1024);
                    if (bytes > 0)
                        res.Write(buffer, 0, bytes);
                } while (bytes>0);
                sr.Close();
                fs_.Close();
            }
            res.Flush();
            res.Close();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            WebProxy proxy=null;
            if (WebRequest.GetSystemWebProxy() as WebProxy  != null)
            {

                proxy = (WebProxy)WebRequest.GetSystemWebProxy();
               
            }else
            {
                proxy_address_tb.Text = "";
                proxy_port_tb.Text = "";
                proxy_lan_chb.Checked = false;
            }
            if (proxy!= null &&proxy.Address.AbsoluteUri != string.Empty)
            {
                proxy_address_tb.Text = proxy.Address.AbsoluteUri;
                proxy_port_tb.Text = proxy.Address.Port.ToString();
                proxy_lan_chb.Checked = proxy.BypassProxyOnLocal;

            }
        }
    }

    class Downloader{
        string url = "";
        int total_parts = 1;
        int my_part = 1;
        string save_location;
       public string logs = "logs:"+Environment.NewLine;
       public double precentage = 0;
        Boolean active = false;
        public long total_my_part_size = 0;
        public long total_my_part_remained = 0;
        
        //proxy
        string proxy_address = null;
        int proxy_port = 0;
        Boolean bypass_lan = false;

        public Downloader(string url , string save_location, int total_parts, int my_part, string proxy_address = null, int proxy_port = 0, Boolean bypass_lan = false)
        {
            this.url = url;
            this.save_location = save_location;
            this.total_parts = total_parts;
            this.my_part = my_part;
        }

        private void log(string log_text)
        {
            logs += log_text  + Environment.NewLine;
        }

        public void start_downloading()
        {
            active = true;

            log("requesting file size");
            HttpWebResponse resp = get_response(url);
            long file_size  =(long) resp.ContentLength  ;
            log("file size:"+ file_size);
            long total_size =(long)Math.Floor((double) (file_size/total_parts));
            log("my part size:" + total_size );

            resp.Close();
            log("requesting file download");

            resp = get_response(url, (long)((total_size * (my_part - 1))));
            if (my_part == total_parts)
                total_size = file_size - (my_part - 1) * total_size;
            System.IO.Stream  stream = resp.GetResponseStream();
            
            int buffer_size = 65536;//64 * 1024
            byte[] buffer = new byte[buffer_size];
            int bytes_read = -1;
            System.IO.FileStream  writer = System.IO.File.OpenWrite(save_location + "." + total_parts + "_from_" + my_part);

            byte[] pre_data = Encoding.UTF8.GetBytes("file_address=" + url + Environment.NewLine + "total_parts=" + total_parts + Environment.NewLine + "my_part=" + my_part + Environment.NewLine
                + "total_file_size=" + file_size + Environment.NewLine + "my_size=" +  total_parts + Environment.NewLine
                + "this_file_start_byte=" + (int)Math.Floor((double)(file_size / total_parts) * (my_part - 1)) + Environment.NewLine
                 + "this_file_bytes=" +total_size + Environment.NewLine);

            
            writer.Write(Encoding.UTF8.GetBytes( pre_data.Length.ToString().PadLeft(10, '0')), 0, 10);
           
            writer.Write(pre_data,0,pre_data.Length);
            total_my_part_size = total_size;
            while (total_size>0 && active)
            {
                
                if (total_size>buffer_size  )
                    bytes_read = web_read( stream,buffer, 0, buffer_size );
                else
                    bytes_read = web_read( stream,buffer, 0, (int) total_size);

                if (bytes_read > 0)
                {
                    writer.Write(buffer, 0, bytes_read);
                    writer.Flush();

                    precentage =(int)( (1 - (total_size / (double)Math.Floor((double)(file_size / total_parts))))*100);
                        //(file_size/total_parts - total_size) / (file_size / total_parts) * 100;
                    total_size -= bytes_read;
                }
                total_my_part_remained = total_size;
            }
            writer.Close();
            stream.Close();
            resp.Close();
        }
        private HttpWebResponse get_response(string url, long offset=0,string proxy_address=null , int proxy_port=0,Boolean bypass_lan=false)//HttpWebRequest request)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                if (proxy_address != null)
                {

                WebProxy myproxy = new WebProxy(proxy_address ,proxy_port );
                myproxy.BypassProxyOnLocal = bypass_lan ;
                request.Proxy = myproxy;
                }
                request.AddRange("bytes",offset );//set starter point 

                return (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                MessageBox.Show(ex.Message + ex.Data);
                return get_response( url,  offset);
                
            }

        }

        private int web_read(Stream stream ,byte[] buffer , int index,int count)
        {
            int read = 0;
            try
            {
               read= stream.Read(buffer, 0, count);
            }
            catch (Exception ex)
            {

                throw;
            }
            return read;
        }
        public void stop()
        {
            active = false;
        }
    }
}
